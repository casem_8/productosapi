﻿using Microsoft.EntityFrameworkCore;
using WebApiCrud.Models;

namespace WebApiCrud.Contexto
{
    public class MyDbContext : DbContext
    {
        public MyDbContext(DbContextOptions options) : base(options)
        {
        }

        // Entidad Producto aqui
        public DbSet<Producto> Productos { get; set; }
    }
}
