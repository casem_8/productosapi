﻿using WebApiCrud.Models;

namespace WebApiCrud.Services
{
    public interface IProductoService
    {
        Task<IEnumerable<Producto>> ObtenerProductos();
        Task<Producto> ObtenerProducto(int id);
        Task<Producto> GuardarProducto(Producto producto);
        Task<bool> EditarProducto(int id, Producto producto);
        Task<bool> EliminarProducto(int id);
        bool ProductoDisponible(int id);
    }
}
