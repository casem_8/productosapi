﻿using Microsoft.EntityFrameworkCore;
using WebApiCrud.Contexto;
using WebApiCrud.Models;

namespace WebApiCrud.Services
{
    public class ProductoService : IProductoService
    {
        private readonly MyDbContext _contexto;

        public ProductoService(MyDbContext contexto)
        {
            _contexto = contexto;
        }

        // Obtener lista de productos
        public async Task<IEnumerable<Producto>> ObtenerProductos()
        {
            return await _contexto.Productos.ToListAsync();
        }

        // Obtener producto por id
        public async Task<Producto> ObtenerProducto(int id)
        {
            return await _contexto.Productos.FindAsync(id);
        }

        // Guardar producto
        public async Task<Producto> GuardarProducto(Producto producto)
        {
            _contexto.Productos.Add(producto);
            await _contexto.SaveChangesAsync();
            return producto;
        }

        // Editar producto
        public async Task<bool> EditarProducto(int id, Producto producto)
        {
            if (id != producto.Id)
            {
                return false;
            }

            _contexto.Entry(producto).State = EntityState.Modified;

            try
            {
                await _contexto.SaveChangesAsync();
                return true;
            }
            catch (DbUpdateConcurrencyException)
            {
                return false;
            }
        }

        // Eliminar producto
        public async Task<bool> EliminarProducto(int id)
        {
            var producto = await _contexto.Productos.FindAsync(id);

            if (producto == null)
            {
                return false;
            }

            _contexto.Productos.Remove(producto);
            await _contexto.SaveChangesAsync();
            return true;
        }

        public bool ProductoDisponible(int id)
        {
            return _contexto.Productos.Any(x => x.Id == id);
        }
    }
}
